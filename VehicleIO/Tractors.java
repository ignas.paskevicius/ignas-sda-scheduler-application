public class Tractors extends Vehicle{
    private int maxPullWeight;

    public Tractors(String type, String brand, String model, double price, int maxPullWeight) {
        super(type, brand, model, price);
        this.maxPullWeight = maxPullWeight;
    }

    public int getMaxPullWeight() {
        return maxPullWeight;
    }

    @Override
    public String toString(){
        return getType() + ": "
                + getBrand() + " " + getModel()
                + ", Price: " + getPrice()
                + ", Max pull weight: " + getMaxPullWeight();
    }
}
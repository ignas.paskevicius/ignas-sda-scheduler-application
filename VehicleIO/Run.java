import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Run {

    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RESET = "\u001B[0m";

    public static void main(String[] args) {
        List<Vehicle> vehicleList = new ArrayList<>();


        //Reading vehicles from files and adding it to list
        File file = new File("target\\classes\\txtFile\\vehicles.txt");

        try(BufferedReader br = new BufferedReader(new FileReader(file))){

            String line = br.readLine();

            while (line != null){

                String[] string = line.split(", ");

                if(string[0].equals("Car")){
                    vehicleList.add(new Cars(string[0], string[1], string[2], Double.parseDouble(string[3]),
                            Integer.parseInt(string[4]), Cars.Transmission.valueOf(string[5]), Cars.Shape.valueOf(string[6])));
                }else if(string[0].equals("Motorcycle")){
                    vehicleList.add(new Motorcycles(string[0], string[1], string[2], Double.parseDouble(string[3]),
                            Integer.parseInt(string[4]), Motorcycles.Shape.valueOf(string[5])));
                }else if(string[0].equals("Tractor")){
                    vehicleList.add(new Tractors(string[0], string[1], string[2], Double.parseDouble(string[3]),
                            Integer.parseInt(string[4])));
                }
                line = br.readLine();
            }
        }catch (IOException e){
            e.printStackTrace();
        }

        System.out.println(ANSI_GREEN + "\nPrinting vehicles list: " + ANSI_RESET);
        vehicleList.forEach(System.out::println);

        //Count the number of cars, motorcycles, tractors
        int carsCount = (int) vehicleList.stream().filter(v -> v instanceof Cars).count();
        System.out.println(ANSI_GREEN + "\nTotal cars in a list: " + ANSI_RESET + carsCount);

        int motorcyclesCount = (int) vehicleList.stream().filter(v -> v instanceof Motorcycles).count();
        System.out.println(ANSI_GREEN + "Total motorcycles in a list: " + ANSI_RESET + motorcyclesCount);

        int tractorsCount = (int) vehicleList.stream().filter(v -> v instanceof Tractors).count();
        System.out.println(ANSI_GREEN + "Total tractors in a list: " + ANSI_RESET + tractorsCount);

        //Count how many vehicles of each brand are there
        List<String> brandsList = vehicleList.stream().map(Vehicle::getBrand).distinct().collect(Collectors.toList());

        System.out.println(ANSI_GREEN + "\nCount of vehicles of each brand:" + ANSI_RESET);
        for(String b : brandsList){
            int count = (int) vehicleList.stream().filter(v -> v.getBrand().equals(b)).count();
            System.out.println(b + ": " + count);
        }

        //Sort cars by price
        System.out.println(ANSI_GREEN + "\nCars sorted by price:" + ANSI_RESET);
        vehicleList.stream().filter(v -> v instanceof Cars)
                .sorted(Comparator.comparing(Vehicle::getPrice))
                .forEach(System.out::println);

        //Sort choppers by top speed
        System.out.println(ANSI_GREEN + "\nChoppers sorted by top speed:" + ANSI_RESET);
        vehicleList.stream()
                .filter(v -> v instanceof Motorcycles)
                .filter(m -> ((Motorcycles) m).getShape().equals(Motorcycles.Shape.CHOPPER))
                .sorted(Comparator.comparing(a -> ((Motorcycles) a).getTopSpeed()))
                .forEach(System.out::println);
    }
}
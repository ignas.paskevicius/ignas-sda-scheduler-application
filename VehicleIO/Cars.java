public class Cars extends Vehicle {
    private Transmission transmission;
    private Shape shape;
    private int topSpeed;

    enum Transmission{
        AUTOMATIC,
        MANUAL
    }

    enum Shape{
        COUPE,
        SEDAN,
        WAGON
    }

    public Cars(String type, String brand, String model, double price, int topSpeed, Transmission transmission, Shape shape) {
        super(type, brand, model, price);
        this.transmission = transmission;
        this.shape = shape;
        this.topSpeed = topSpeed;
    }


    public Transmission getTransmission() {
        return transmission;
    }

    public Shape getShape() {
        return shape;
    }

    public int getTopSpeed() {
        return topSpeed;
    }

    @Override
    public String toString(){
        return getType() + ": "
                + getBrand() + " " + getModel()
                + ", Price: " + getPrice()
                + ", Top speed: " + getTopSpeed()
                + ", Transmission: " + getTransmission()
                + ", Shape: " + getShape();
    }
}
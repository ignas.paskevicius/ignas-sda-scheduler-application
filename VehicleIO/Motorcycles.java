public class Motorcycles extends Vehicle {
    private Shape shape;
    private int topSpeed;

    enum Shape{
        CHOPPER,
        CRUISER,
        ENDURO
    }

    public Motorcycles(String type, String brand, String model, double price, int topSpeed, Shape shape) {
        super(type, brand, model, price);
        this.shape = shape;
        this.topSpeed = topSpeed;
    }

    public Shape getShape() {
        return shape;
    }

    public int getTopSpeed() {
        return topSpeed;
    }

    @Override
    public String toString() {
        return getType() + ": "
                + getBrand() + " " + getModel()
                + ", Price: " + getPrice()
                + ", Top speed: " + getTopSpeed()
                + ", Shape: " + getShape();
    }
}
public class Vehicle {
    private String type;
    private String brand;
    private String model;
    private double price;

    public Vehicle(String type, String brand, String model, double price){
        this.type = type;
        this.brand = brand;
        this.model = model;
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public double getPrice() {
        return price;
    }
}
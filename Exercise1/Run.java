import java.time.LocalDate;
import java.time.Period;
import java.util.*;
import java.util.stream.Collectors;

public class Run {

    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLUE = "\u001B[34m";

    public static void main(String[] args) throws MaximumNumberOfStudentsReached {

        List<Student> studentsList = new ArrayList<>();

        Student student1 = new Student("Laurie", "Harison", "1978-05-01", false);
        studentsList.add(student1);

        Student student2 = new Student("Raul", "Powell", "1994-02-12", true);
        studentsList.add(student2);

        Student student3 = new Student("Larry", "Goodwin", "1980-09-15", false);
        studentsList.add(student3);

        Student student4 = new Student("Hope", "Kelly", "1992-07-01", false);
        studentsList.add(student4);

        Student student5 = new Student("Bonnie", "Summers", "2002-05-01", false);
        studentsList.add(student5);

        Student student6 = new Student("Patty", "Simon", "2000-12-20", true);
        studentsList.add(student6);

        Student student7 = new Student("Virgil", "Baker", "1986-06-15", true);
        studentsList.add(student7);

        Student student8 = new Student("Jamie", "Ellis", "1999-11-06", false);
        studentsList.add(student8);

        Student student9 = new Student("Paula", "Gill", "1995-02-02", false);
        studentsList.add(student9);

        Student student10 = new Student("Jody", "Hunt", "1991-04-26", true);
        studentsList.add(student10);

        Student student11 = new Student("Neal", "Cruz", "2001-10-18", false);
        studentsList.add(student11);

        Student student12 = new Student("Kristina", "Parks", "1994-03-13", true);
        studentsList.add(student12);

        Student student13 = new Student("Della", "Frank", "1989-05-09", false);
        studentsList.add(student13);

        Student student14 = new Student("Al", "Floyd", "1984-05-05", true);
        studentsList.add(student14);

        Student student15 = new Student("Brent", "Wills", "1997-08-09", false);
        studentsList.add(student15);

        //Creating trainers and adding to list
        List<Trainer> trainersList = new ArrayList<>();

        Trainer trainer1 = new Trainer.Builder()
                .firstName("Jonas")
                .lastName("Jonauskas")
                .dateOfBirth("1984-04-02")
                .build();

        trainersList.add(trainer1);

        Trainer trainer2 = new Trainer.Builder()
                .firstName("Petras")
                .lastName("Petrauskas")
                .dateOfBirth("1980-08-20")
                .isAuthorized(true)
                .build();

        trainersList.add(trainer2);

        Trainer trainer3 = new Trainer.Builder()
                .firstName("Antanas")
                .lastName("Antanaitis")
                .dateOfBirth("1990-01-02")
                .isAuthorized(false)
                .build();

        trainersList.add(trainer3);

        //Creating groups and adding it to list
        List<Group> groupsList = new ArrayList<>();

        Group group1 = new Group("JavaVilnius1", null, new ArrayList<>());
        Group group2 = new Group("JavaVilnius2", null, new ArrayList<>());
        Group group3 = new Group("JavaVilnius3", null, new ArrayList<>());
        Group group4 = new Group("JavaVilnius4", null, new ArrayList<>());

        groupsList.add(group1);
        groupsList.add(group2);
        groupsList.add(group3);
        groupsList.add(group4);

        //Assign trainer to each group
        groupsList.get(0).setTrainer(trainersList.get(0));
        groupsList.get(1).setTrainer(trainersList.get(0));
        groupsList.get(2).setTrainer(trainersList.get(1));
        groupsList.get(3).setTrainer(trainersList.get(2));

        //Assign 2/3 students to each group
        groupsList.get(0).addToStudentsList(studentsList.get(0));
        groupsList.get(0).addToStudentsList(studentsList.get(1));
        groupsList.get(0).addToStudentsList(studentsList.get(2));

        groupsList.get(1).addToStudentsList(studentsList.get(3));
        groupsList.get(1).addToStudentsList(studentsList.get(4));
        groupsList.get(1).addToStudentsList(studentsList.get(5));

        groupsList.get(2).addToStudentsList(studentsList.get(6));
        groupsList.get(2).addToStudentsList(studentsList.get(7));
        groupsList.get(2).addToStudentsList(studentsList.get(8));

        groupsList.get(3).addToStudentsList(studentsList.get(9));
        groupsList.get(3).addToStudentsList(studentsList.get(10));
        groupsList.get(3).addToStudentsList(studentsList.get(11));

        System.out.println(ANSI_GREEN + "Printing all groups:" + ANSI_RESET);
        groupsList.forEach(System.out::println);

        //Ensure the fact that a group will only have distinct students
        System.out.println(ANSI_GREEN + "Trying to add a student who is already in a group:" + ANSI_RESET);
        groupsList.get(0).addToStudentsList(studentsList.get(0));

        //Ensure the fact that a group will only have a maximum of 5 students
        /*System.out.println(ANSI_GREEN + "Trying to add more than 5 users to group:" + ANSI_RESET);
        groupsList.get(0).addToStudentsList(studentsList.get(4));
        groupsList.get(0).addToStudentsList(studentsList.get(5));
        groupsList.get(0).addToStudentsList(studentsList.get(6));*/


        //Display all students in a group sorted alphabetically by lastName
        System.out.println(ANSI_GREEN + "\nSorted students in a group "+ groupsList.get(0).getGroup() +" by last name:" + ANSI_RESET);
        groupsList.get(0).getStudentList().stream().sorted((Comparator.comparing(Person::getLastName))).forEach(System.out::println);


        //Display the group with the maximum number of students
        groupsList.get(1).addToStudentsList(studentsList.get(12));
        groupsList.get(1).addToStudentsList(studentsList.get(13));

        System.out.println(ANSI_GREEN + "\nGroups with maximum number of students" + ANSI_RESET);
        groupsList.stream().filter(g -> g.getStudentList().size() == 5).forEach(System.out::println);

        //Display all students younger than 25, from all groups
        System.out.println(ANSI_GREEN + "All groups with students younger than 25:" + ANSI_RESET);
        for(Group g : groupsList){
            List<Student> youngerStudents = g.getStudentList().stream()
                    .filter(s -> Period.between(LocalDate.parse(s.getDateOfBirth()), LocalDate.now()).getYears() < 25)
                    .collect(Collectors.toList());
            if(!youngerStudents.isEmpty()) {
                System.out.println(ANSI_BLUE + "\nStudents younger than 25 years from group " + g.getGroup() + ":" + ANSI_RESET);
                youngerStudents.forEach(System.out::println);
            }else{
                System.out.println(ANSI_BLUE + "\nThere are no students younger than 25 in a group " + g.getGroup() + ANSI_RESET);
            }
        }

        //Display all students grouped by trainer that teaches to them
        System.out.println(ANSI_GREEN + "\nAll students grouped by trainer:" + ANSI_RESET);
        Map<String, List<Student>> teachedBy = new HashMap<>();
        for(Group g : groupsList){
            List<Student> student = new ArrayList<>(g.getStudentList());
            if(teachedBy.containsKey(g.getTrainer().getFirstName() + " " + g.getTrainer().getLastName())){
                List<Student> tmpList = teachedBy.get(g.getTrainer().getFirstName() + " " + g.getTrainer().getLastName());
                tmpList.addAll(student);
            }else {
                teachedBy.put(g.getTrainer().getFirstName() + " " + g.getTrainer().getLastName(), student);
            }

        }
        System.out.println(teachedBy);

        //Display all students with previous java knowledge
        System.out.println(ANSI_GREEN + "\nStudents with previous Java knowledge:" + ANSI_RESET);
        studentsList.stream().filter(Student::isHasPreviousJavaKnowledge).collect(Collectors.toList()).forEach(System.out::println);

        //Display the group with the highest number of students with no previous java knowledge
        int noPreviousKnowledgeCount = 0;
        int groupIndex = 0;
        for(Group g : groupsList){
            int count = (int) g.getStudentList().stream().filter(s -> !s.isHasPreviousJavaKnowledge()).count();
            if(count > noPreviousKnowledgeCount){
                groupIndex = groupsList.indexOf(g);
                noPreviousKnowledgeCount = count;
            }
        }
        System.out.println(ANSI_GREEN + "\nGroup with highest number of students with no java knowledge:" + ANSI_RESET);
        System.out.println(groupsList.get(groupIndex));

        //Remove all the students younger than 20 from all groups
        for(Group g : groupsList){
            g.getStudentList().removeIf(s -> (Period.between(LocalDate.parse(s.getDateOfBirth()), LocalDate.now()).getYears() < 25));
        }
        System.out.println(ANSI_GREEN + "\nGroups with no students younger than 20:" + ANSI_RESET);
        groupsList.forEach(System.out::println);
    }
}
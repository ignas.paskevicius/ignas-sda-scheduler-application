public class Trainer extends Person {
    private boolean isAuthorized;

    public Trainer(Builder build) {
        super(build.firstName, build.lastName, build.dateOfBirth);
        this.isAuthorized = build.isAuthorized;
    }



    public static class Builder{
        private String firstName;
        private String lastName;
        private String dateOfBirth;
        private boolean isAuthorized;
        public Trainer build(){
            return new Trainer(this);
        }

        public Builder firstName(String firstName){
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName){
            this.lastName = lastName;
            return this;
        }

        public Builder dateOfBirth(String dateOfBirth){
            this.dateOfBirth = dateOfBirth;
            return this;
        }

        public Builder isAuthorized(boolean isAuthorized){
            this.isAuthorized = isAuthorized;
            return this;
        }
    }

    @Override
    public String toString(){
        return getFirstName() + " " + getLastName() + " " + getDateOfBirth();
    }

}
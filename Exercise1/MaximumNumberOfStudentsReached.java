public class MaximumNumberOfStudentsReached extends Exception {
    public MaximumNumberOfStudentsReached(){
        super("There are 5 students in a group already!");
    }
}
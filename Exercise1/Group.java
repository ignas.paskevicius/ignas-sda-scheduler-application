import java.util.ArrayList;
import java.util.List;

public class Group {
    private String group;
    private Trainer trainer;
    private List<Student> studentList = new ArrayList<>();

    public Group(String group, Trainer trainer, List<Student> studentList){
        this.group = group;
        this.trainer = trainer;
        this.studentList = studentList;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void addToStudentsList(Student s) throws MaximumNumberOfStudentsReached {
        if (studentList.size() == 5) {
            throw new MaximumNumberOfStudentsReached();
        }else if (studentList.contains(s)) {
            System.out.println("This student already exists in this group!");
        } else {
            studentList.add(s);
        }
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    @Override
    public String toString(){
        return "Group: " + group + ";\nTrainer: " + trainer + ";\n" + "Students list: " + studentList + "\n";
    }
}
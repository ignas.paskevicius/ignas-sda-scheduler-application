public class Student extends Person implements Comparable {

    private boolean hasPreviousJavaKnowledge;

    public Student(String firstName, String lastName, String dateOfBirth, boolean hasPreviousJavaKnowledge) {
        super(firstName, lastName, dateOfBirth);
        this.hasPreviousJavaKnowledge = hasPreviousJavaKnowledge;
    }

    public boolean isHasPreviousJavaKnowledge() {
        return hasPreviousJavaKnowledge;
    }

    public void setHasPreviousJavaKnowledge(boolean hasPreviousJavaKnowledge) {
        this.hasPreviousJavaKnowledge = hasPreviousJavaKnowledge;
    }

    @Override
    public String toString(){
        return getFirstName() + " " + getLastName() + " " + getDateOfBirth() + " Previous Java knowledge: " + isHasPreviousJavaKnowledge();
    }


    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
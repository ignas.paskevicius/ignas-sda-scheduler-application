import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class Product {
    private String name;
    private int stock;
    private double price;

    public Product(String name, int stock, double price) {
        this.name = name;
        this.stock = stock;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getStock() {
        return stock;
    }

    public double getPrice() {
        return price;
    }

    public static List<Product> addProductsToList(){

        JsonParser parser = new JsonParser();
        JsonObject cont = null;
        try(FileReader reader = new FileReader("target\\classes\\jsonFile\\VendingMachineProducts.json")){
            cont = parser.parse(reader).getAsJsonObject();
        }catch (IOException e){
            e.printStackTrace();
        }
        JsonArray arr = cont.get("productStock").getAsJsonArray();
        List<Product> productsList = new Gson().fromJson(arr, new TypeToken<List<Product>>() { }.getType());

        return productsList;
    }

    public void reduceStock(List<Product> productList){
        stock--;
        if(stock == 0){
            productList.remove(this);
        }
    }

    @Override
    public String toString(){
        return getName() + ", price: " + getPrice() + ", amount: " + getStock();
    }
}
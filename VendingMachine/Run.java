import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Scanner;

public class Run {

    private static DecimalFormat df2 = new DecimalFormat("#.##");

    public static void main(String[] args) throws IOException {
        List<Product> productList = Product.addProductsToList();

        do {
            System.out.println("\n*** AVAILABLE PRODUCTS IN VENDING MACHINE ***");
            for (Product p : productList) {
                System.out.println((productList.indexOf(p) + 1) + ". " + p);
            }

            Scanner scanner = new Scanner(System.in);
            System.out.print("\nSelect a product by entering a number from menu: ");
            int productFromMenu = scanner.nextInt();
            Product selectedProduct = productList.get((productFromMenu - 1));

            System.out.println("Selected product: " + selectedProduct.getName());

            System.out.println("\nInsert coins to machine, enter 0 when you finish: ");
            double sumOfCoins = 0;
            double insertCoin;
            while (true) {
                insertCoin = scanner.nextDouble();
                sumOfCoins += insertCoin;
                System.out.println("Sum of coins: " + sumOfCoins);
                if (insertCoin == 0) {
                    if (sumOfCoins >= selectedProduct.getPrice()) {
                        break;
                    } else {
                        System.out.println("Not enough to complete a purchase, insert more coins!");
                    }
                }
            }

            System.out.print("\nIf you want to complete complete a purchase enter 'Y' or enter 'N' to refund: ");
            String confirmPurchase = scanner.next();

            if (confirmPurchase.equalsIgnoreCase("Y")) {
                System.out.println("*** PURCHASE COMPLETED ***");
                System.out.println("PRODUCT: " + selectedProduct.getName());
                System.out.println("CHANGE: " + df2.format(sumOfCoins - selectedProduct.getPrice()));
                selectedProduct.reduceStock(productList);
            } else if (confirmPurchase.equalsIgnoreCase("N")) {
                System.out.println("*** PURCHASE CANCELED ***");
                System.out.println("REFUND: " + df2.format(sumOfCoins));
            }

            System.out.println("Enter 'Y' to choose another product or 'N' to finish");
            String continueOrNot = scanner.next();
            if(continueOrNot.equalsIgnoreCase("N")){
                break;
            }
        }while(true);
    }
}